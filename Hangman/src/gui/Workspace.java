package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import propertymanager.PropertyManager;
import ui.AppGUI;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

import java.awt.*;
import java.awt.Color;
import java.io.IOException;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee, Michael Kauftheil
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              alphabet;          // text area displaying all letters in the alphabet and which ones are guessed
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    Button            hintButton;        // the button to provide a hint for "difficult" words
    VBox              hangmanDrawing;    // the panel which will contain the canvas to draw the hangman
    Pane              canvas;            // the canvas to draw shapes on
    HangmanController controller;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));
        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);
        figurePane = new BorderPane();
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent");
        alphabet = new HBox();
        remainingGuessBox = new HBox();
        hintButton = new Button("HINT!");
        hintButton.setVisible(false);
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, alphabet, hintButton);
        hangmanDrawing = new VBox();
        canvas = new Pane();
        canvas.setPrefSize(300,300);
        hangmanDrawing.getChildren().add(canvas);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(hangmanDrawing, figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
        hintButton.setOnMouseClicked(e -> controller.giveHint());

    }

    /**
     * This method redraws the hangman depending on how many bad guesses have been made
     * @param remainingGuesses
     */
    public void drawHangman(int remainingGuesses)
    {
        canvas.getChildren().clear();
        Line line;
        if (remainingGuesses < 10) //noose part 1
        {
            line = new Line(20,290,280,290);
            canvas.getChildren().add(line);
        }
        if (remainingGuesses < 9) //noose part 2
        {
            line = new Line(20,40,20,290);
            canvas.getChildren().add(line);
        }
        if (remainingGuesses < 8) //noose part 3
        {
            line = new Line(20,40,200,40);
            canvas.getChildren().add(line);
        }
        if (remainingGuesses < 7) //noose part 4
        {
            line = new Line(200,40,200,60);
            canvas.getChildren().add(line);
        }
        if (remainingGuesses < 6) //head
        {
            Circle circle = new Circle(200,100,40,javafx.scene.paint.Color.BLACK);
            canvas.getChildren().add(circle);
        }
        if (remainingGuesses < 5) //torso
        {
            line = new Line(200,140,200,200);
            canvas.getChildren().add(line);
        }
        if (remainingGuesses < 4) //left arm
        {
            line = new Line(200,170,155,215);
            canvas.getChildren().add(line);
        }
        if (remainingGuesses < 3) //right arm
        {
            line = new Line(200,170,245,215);
            canvas.getChildren().add(line);
        }
        if (remainingGuesses < 2) //left leg
        {
            line = new Line(200,200,170,260);
            canvas.getChildren().add(line);
        }
        if (remainingGuesses < 1) //right leg
        {
            line = new Line(200,200,230,260);
            canvas.getChildren().add(line);
        }
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        alphabet = new HBox();
        hintButton = new Button("HINT!");
        hintButton.setOnMouseClicked(e -> controller.giveHint());
        hintButton.setVisible(false);
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, alphabet, hintButton);
        hangmanDrawing = new VBox();
        canvas = new Pane();
        canvas.setPrefSize(300,300);
        hangmanDrawing.getChildren().add(canvas);
        bodyPane.getChildren().setAll(hangmanDrawing, figurePane, gameTextsPane);
    }
}
